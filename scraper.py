#! /bin/python3
import requests
import datetime
import configparser
import time
from rich import print, pretty
import os


def getData(config: configparser.ConfigParser):
    connections = config["GENERAL"]["connections"]
    stations = config["GENERAL"]["stations"]
    nextStations = []
    if stations:
        for station in stations.split(","):
            url = f"https://www.vrn.de/mngvrn/XML_DM_REQUEST?depType=stopEvents&includeCompleteStopSeq=1&limit=5&locationServerActive=1&mode=direct&name_dm={station}&outputFormat=json&type_dm=any&useOnlyStops=1&useRealtime=1"
            r = requests.get(url, timeout=10)
            if r.status_code == 200:
                j = r.json()
                departurelist = j.get("departureList")
                if departurelist:
                    for ele in departurelist:
                        if ele.get("realDateTime"):
                            time = ele.get("realDateTime")
                        else:
                            time = ele.get("dateTime")
                        formattedTime = datetime.datetime(
                            int(time.get("year")),
                            int(time.get("month")),
                            int(time.get("day")),
                            int(time.get("hour")),
                            int(time.get("minute")),
                        )
                        nextStations.append(
                            {
                                "name": ele.get("nameWO"),
                                "to": ele.get("servingLine").get("direction"),
                                "time": formattedTime.strftime("%H:%M"),
                                "relTime": (
                                    formattedTime - datetime.datetime.now()
                                ).total_seconds()
                                / 60,
                                "line": ele.get("servingLine").get("symbol"),
                            }
                        )
    elif connections:
        for connection in connections.split(","):
            src = connection.split("-")[0]
            dst = connection.split("-")[1]
            url = f"https://www.vrn.de/mngvrn/XML_TRIP_REQUEST2?changeSpeed=normal&coordOutputFormat=EPSG:4326&cycleSpeed=14&deleteITPTWalk=0&exclMOT_15=1&exclMOT_16=1&excludedMeans=checkbox&itOptionsActive=1&itPathListActive=1&locationServerActive=1&name_destination={dst}&name_origin={src}&outputFormat=json&ptMacro=true&ptOptionsActive=1&routeType=leasttime&strictMode=0&trITMOT=100&trITMOTvalue=15&type_destination=any&type_origin=any&useElevationData=1&useRealtime=1&useUT=1&useUnifiedTickets=1&wheelchairSpaceStop=0"
            r = requests.get(url, timeout=10)
            if r.status_code == 200:
                j = r.json()
                trips = j.get("trips")
                if trips:
                    for ele in trips:
                        time = ele.get("legs")[0].get("points")[0].get("dateTime")
                        if time.get("rtDate") and time.get("rtTimeSec"):
                            formattedTime = datetime.datetime.combine(
                                datetime.datetime.date(
                                    datetime.datetime.strptime(
                                        time.get("rtDate"), "%d.%m.%Y"
                                    )
                                ),
                                datetime.datetime.time(
                                    datetime.datetime.strptime(
                                        time.get("rtTimeSec"), "%H:%M:%S"
                                    )
                                ),
                            )
                        else:
                            formattedTime = datetime.datetime.combine(
                                datetime.datetime.date(
                                    datetime.datetime.trptime(
                                        time.get("date"), "%d.%m.%Y"
                                    )
                                ),
                                datetime.datetime.time(
                                    datetime.datetime.strptime(
                                        time.get("timeSec"), "%H:%M:%S"
                                    )
                                ),
                            )
                        nextStations.append(
                            {
                                "name": ele.get("legs")[0]
                                .get("points")[0]
                                .get("nameWO"),
                                "to": ele.get("legs")[0].get("mode").get("destination"),
                                "time": formattedTime.strftime("%H:%M"),
                                "relTime": (
                                    formattedTime - datetime.datetime.now()
                                ).total_seconds()
                                / 60,
                                "line": ele.get("legs")[0].get("mode").get("symbol"),
                            }
                        )

    nextStations = sorted(nextStations, key=lambda x: x["relTime"], reverse=True)
    return nextStations


def printData(data: []):
    if data:
        for ele in data:
            print(
                f"{ele.get('line')}-{ele.get('time')} ({ele.get('relTime'):.1f} min)-[#FFFF00]{ele.get('to')}[/]"
            )
    else:
        print(
            "There are no departures in the near future or there is no Internet. Stay tuned!"
        )
    print(f"The Time now is {datetime.datetime.now().strftime('%H:%M')}.")


def main():
    config = configparser.ConfigParser()
    config.read("app.config")
    pretty.install()
    while True:
        try:
            os.system("cls" if os.name == "nt" else "clear")
            # try:
            data = getData(config)
            printData(data)
            # except Exception as e:
            #     print(str(e))
            time.sleep(60)
        except Exception:
            pass


if __name__ == "__main__":
    main()
