# train proclaim

Gets the data from the next train station(s) and shows them on a terminal. Refreshes every 1 minutes. It is using the VRN API, but it can be used for other operators as well, because they provide the real-time data for more operators.

## Usage

Change the app.config.example to app.config and change the station to your desired one. You can find the via checking the Console when Using the vrn Abfahrtsmonitor (https://www.vrn.de/index.html - look for the call 'XML_DM_REQUEST' and search in the response after 'dm') or by using the API by DB (https://developer.deutschebahn.com/store/apis/info?name=StaDa-Station_Data&version=v2&provider=DBOpenData - you need to sign up).

If you set the stations (Syntax: ***station1***,***station2***,***station3***) field, you get all the next departures on a train for the next minutes. The departures (Syntax: ***src1***-***dst1***,***src2***-***dst2***) gives you the departure time for all the following connections.
